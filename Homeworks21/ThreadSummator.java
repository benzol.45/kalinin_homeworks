package com.company.HomeWorks21;

import java.util.Arrays;

public class ThreadSummator {

    public static int oneThradSum(int[] array) {
        return Arrays.stream(array).sum();
    }

    public static int multithradSum(int[] array, int numerOfThread) {
        if (numerOfThread <= 0) {
            throw new IllegalArgumentException("Can't run work without threads");
        }

        int result = 0;

        Thread[] threadPool = new Thread[numerOfThread];
        int numerOfElementsOnThread = array.length / numerOfThread;

        for (int i = 0; i < numerOfThread; i++) {
            if (i != numerOfThread - 1) {
                threadPool[i] = new Summator(array, i * numerOfElementsOnThread, numerOfElementsOnThread);
            } else {
                //latest thread
                threadPool[i] = new Summator(array, i * numerOfElementsOnThread, array.length - (i) * numerOfElementsOnThread);
            }
        }

        try {
            for (Thread thread : threadPool) {
                thread.start();
            }

            for (Thread thread : threadPool) {
                thread.join();
            }

            for (Thread thread : threadPool) {
                result += ((Summator) thread).getResult();
            }
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }

        return result;
    }
}
