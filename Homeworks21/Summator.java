package com.company.HomeWorks21;

import java.util.Arrays;

public class Summator extends Thread {
    final int[] array;
    final int fromElement;
    final int numerOfElement;
    int result = 0;

    public Summator(int[] array, int fromElement, int numerOfElement) {
        this.array = array;
        this.fromElement = fromElement;
        this.numerOfElement = numerOfElement;
    }

    @Override
    public void run() {
        result = Arrays.stream(array)
                .skip(fromElement)
                .limit(numerOfElement).sum();
    }

    public int getResult() {
        return this.result;
    }
}
