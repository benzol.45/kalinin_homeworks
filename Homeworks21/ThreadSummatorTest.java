package com.company.HomeWorks21;

import org.junit.Test;

import java.util.Arrays;
import java.util.Random;

import static org.junit.Assert.*;

public class ThreadSummatorTest {

    @Test
    public void multithradSum() {
        int[] emptyArray = new int[0];
        assertEquals(0, ThreadSummator.oneThradSum(emptyArray));
        assertEquals(0, ThreadSummator.multithradSum(emptyArray, 100));

        assertThrows(IllegalArgumentException.class, () -> {ThreadSummator.multithradSum(emptyArray, 0);});
        assertThrows(IllegalArgumentException.class, () -> {ThreadSummator.multithradSum(emptyArray, -100);});

        int[] testArray = new int[1070];
        Arrays.fill(testArray, 1);
        assertEquals(1070, ThreadSummator.oneThradSum(testArray));
        assertEquals(1070, ThreadSummator.multithradSum(testArray, 1));
        assertEquals(1070, ThreadSummator.multithradSum(testArray, 100));

        Random random = new Random();
        for (int i = 0; i < testArray.length; i++) {
            testArray[i] = random.nextInt(100);
        }
        assertEquals(ThreadSummator.oneThradSum(testArray), ThreadSummator.multithradSum(testArray, 1));
        assertEquals(ThreadSummator.oneThradSum(testArray), ThreadSummator.multithradSum(testArray, 100));
    }
}