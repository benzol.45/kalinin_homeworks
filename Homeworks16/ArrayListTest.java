package com.company.HomeWorks16;

import org.junit.Test;

import java.lang.reflect.Field;

import static org.junit.Assert.*;

public class ArrayListTest {
    @Test
    public void removeAt_IndexOutOfBounds() {
        ArrayList<Integer> testedList = new ArrayList<>();
        testedList.add(45);
        testedList.add(78);

        ArrayList<Integer> expectedList = new ArrayList<>();
        expectedList.add(45);
        expectedList.add(78);

        assertThrows(IndexOutOfBoundsException.class, () -> testedList.removeAt(-1));
        assertThrows(IndexOutOfBoundsException.class, () -> testedList.removeAt(5));

        for (int i = 0; i < 2; i++) {
            assertTrue(testedList.get(i) == expectedList.get(i));
        }

        ArrayList<Integer> cleanList = new ArrayList<>();
        assertThrows(IndexOutOfBoundsException.class, () -> cleanList.removeAt(0));
        assertThrows(IndexOutOfBoundsException.class, () -> cleanList.removeAt(1));
    }

    @Test
    public void removeAt_MarselExample() {
        ArrayList<Integer> testedList = new ArrayList<>();
        testedList.add(45);
        testedList.add(78);
        testedList.add(10);
        testedList.add(17);
        testedList.add(89);

        ArrayList<Integer> expectedList = new ArrayList<>();
        expectedList.add(45);
        expectedList.add(78);
        expectedList.add(10);
        expectedList.add(89);
        Integer sizeExpectedList = 4;

        testedList.removeAt(3);

        Integer sizeTestedList = null;
        try {
            Field fieldSizeTestedList = testedList.getClass().getDeclaredField("size");
            fieldSizeTestedList.setAccessible(true);
            sizeTestedList = (Integer) fieldSizeTestedList.get(testedList);
        } catch (Exception e) {
            throw new IllegalArgumentException();
        }

        assertEquals(sizeExpectedList, sizeTestedList);

        for (int i = 0; i < sizeExpectedList; i++) {
            assertTrue(testedList.get(i) == expectedList.get(i));
        }


    }
}