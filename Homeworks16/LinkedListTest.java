package com.company.HomeWorks16;

import org.junit.Test;

import static org.junit.Assert.*;

public class LinkedListTest {

    @Test
    public void get_IndexOutOfBounds() {
        LinkedList<Integer> testedList = new LinkedList<>();
        for (int i = 0; i < 42; i++) {
            testedList.add(i);
        }
        assertThrows(IndexOutOfBoundsException.class, () -> testedList.get(-1));
        assertThrows(IndexOutOfBoundsException.class, () -> testedList.get(1000));
    }

    @Test
    public void get() {
        LinkedList<Integer> testedList = new LinkedList<>();
        for (int i = 0; i < 42; i++) {
            testedList.add(i);
        }

        assertEquals(0, testedList.get(0).intValue());
        assertEquals(41, testedList.get(41).intValue());
        assertEquals(20, testedList.get(20).intValue());


    }
}