package com.company;

/*Реализовать программу на Java, которая для последовательности чисел, оканчивающихся на -1 выведет самую минимальную цифру, встречающуюся среди чисел последовательности.

        Например:

        345
        298
        456
        -1

        Ответ: 2*/

import java.util.Scanner;

public class HomeWorks05 {
    public static void main(String[] args) {
        int minDigit=10;
        Scanner scanner = new Scanner(System.in);
        int currentNumer;

        while (true) {
            currentNumer = scanner.nextInt();
            if (currentNumer==-1) {
                break;
            }

            minDigit=getMinDigit(currentNumer)<minDigit?getMinDigit(currentNumer):minDigit;
        }

        if (minDigit==10) {
            System.out.println("The entered sequence contains no elements");
        } else {
            System.out.println("Minimal digit: " + minDigit);
        }
    }

    public static int getMinDigit(int numer) {
        numer=Math.abs(numer);
        int minDigit=9;
        do {
            minDigit=(numer%10<minDigit)?numer%10:minDigit;
            numer/=10;
        } while (numer>0);

        return minDigit;
    }
}
