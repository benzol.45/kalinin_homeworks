package com.company;

import org.junit.jupiter.api.Test;

import static org.junit.Assert.*;


class HomeWorks05Test {
    @Test
    void getMinDigit() {
        assertEquals(HomeWorks05.getMinDigit(0),0);
        assertEquals(HomeWorks05.getMinDigit(1),1);
        assertEquals(HomeWorks05.getMinDigit(100),0);
        assertEquals(HomeWorks05.getMinDigit(-100),0);
        assertEquals(HomeWorks05.getMinDigit(-123),1);
        assertEquals(HomeWorks05.getMinDigit(345),3);
        assertEquals(HomeWorks05.getMinDigit(298),2);
        assertEquals(HomeWorks05.getMinDigit(456),4);
    }
}