package com.company.HomeWorks10;

public class Rectangle extends Figure {
    private final double a,b;

    public double getA() {
        return a;
    }

    public Rectangle(int x, int y, double a, double b) {
        super(x,y);
        this.a = a;
        this.b = b;
    }

    @Override
    public double getPerimeter() {
        return a*b;
    }
}
