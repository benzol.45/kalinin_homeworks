package com.company.HomeWorks10;

public class Square extends Rectangle implements Moveble{

    public Square(int x, int y, double a) {
        super(x,y,a,a);
    }

    public Square(double a) {
        super(0,0,a,a);
    }

    @Override
    public void moveToXY(int x, int y) {
        setX(x);
        setY(y);
    }

    @Override
    public double getPerimeter() {
        return Math.pow(getA(),2);
    }
}
