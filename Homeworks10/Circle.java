package com.company.HomeWorks10;

public class Circle extends Ellipse implements Moveble{

    public Circle(int x, int y, double r) {
        super(x,y,r,r);
    }

    public Circle(double r) {
        super(0,0,r,r);
    }

    @Override
    public void moveToXY(int x, int y) {
        setX(x);
        setY(y);
    }

    @Override
    public double getPerimeter() {
        return Math.PI*Math.pow(getR1(),2);
    }
}
