package com.company.HomeWorks10;

public class Ellipse extends Figure {
    private final double r1,r2;

    public double getR1() {
        return r1;
    }

    public Ellipse(int x, int y, double r1, double r2) {
        super(x,y);
        this.r1 = r1;
        this.r2 = r2;
    }

    @Override
    public double getPerimeter() {
        return 4d*(Math.PI*r1*r2+Math.pow((r1-r2),2))/(r1+r2);
    }
}
