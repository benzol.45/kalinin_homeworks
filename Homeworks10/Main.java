package com.company.HomeWorks10;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        Moveble[] movebles = new Moveble[] {new Circle(1),new Circle(1,1,1), new Circle(-1,-1,1),
                                            new Square(1),new Square(1,1,1), new Square(-1,-1,1)};

        System.out.println("Before");
        Arrays.stream(movebles).forEach(o->System.out.print((Figure)o + "  "));

        Arrays.stream(movebles).forEach(o->o.moveToXY(100,100));

        System.out.println("\nAfter");
        Arrays.stream(movebles).forEach(o->System.out.print((Figure)o + "  "));

    }
}
