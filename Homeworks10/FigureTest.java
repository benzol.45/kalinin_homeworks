package com.company.HomeWorks10;

import org.junit.Test;

import java.util.Arrays;

import static org.junit.Assert.assertEquals;

public class FigureTest {

    @Test
    public void getPerimeter_Ellipse() {
        Figure ellipse = new Ellipse(0,0,3,2);
        assertEquals(15.8796d,ellipse.getPerimeter(),1d/10000);
    }

    @Test
    public void getPerimeter_Circle() {
        Figure circle = new Circle(2);
        assertEquals(12.5663d,circle.getPerimeter(),1d/10000);
    }

    @Test
    public void getPerimeter_Rectangle() {
        Figure rectangle = new Rectangle(0,0,3,2);
        assertEquals(6d,rectangle.getPerimeter(),1d/10000);
    }

    @Test
    public void getPerimeter_Square() {
        Figure square = new Square(3);
        assertEquals(9d,square.getPerimeter(),1d/10000);
    }

    @Test
    public void geMoveble() {
        Moveble[] movebles = new Moveble[] {new Circle(1),new Circle(1,1,1), new Circle(-1,-1,1),
                                            new Square(1),new Square(1,1,1), new Square(-1,-1,1)};
        Arrays.stream(movebles).forEach(a->a.moveToXY(2,3));
        for (Moveble moveble: movebles){
            Figure f = (Figure)moveble;
            assertEquals(2,f.getX());
            assertEquals(3,f.getY());
        }
    }
}