package com.company.HomeWorks13;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        int[] array = {0, 1, 2, 3, 4, 5, 6, 7, 8, 10, 11, 20, 21, 30, 31, 40, 41, 50, 51, -2, -3, -4, -10, -11, -12};
        
        ByCondition byCondition = o -> {
            if (o % 2 != 0) {
                return false;
            }

            o = Math.abs(o);
            int sum = 0;
            while (o > 0) {
                sum += o % 10;
                o = o / 10;
            }
            return sum % 2 == 0;
        };
        System.out.println(Arrays.toString(Sequence.filter(array, byCondition)));
    }
}
