package com.company.HomeWorks13;

@FunctionalInterface
public interface ByCondition {
    boolean isOk(int number);
}