package com.company.HomeWorks09;

public class Circle extends  Ellipse{

    public Circle(double r) {
        super(r,r);
    }

    @Override
    public double getPerimeter() {
        return Math.PI*Math.pow(getR1(),2);
    }
}
