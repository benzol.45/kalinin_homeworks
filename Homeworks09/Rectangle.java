package com.company.HomeWorks09;

public class Rectangle extends Figure{
    private final double a,b;

    public double getA() {
        return a;
    }

    public Rectangle(double a, double b) {
        this.a = a;
        this.b = b;
    }

    @Override
    public double getPerimeter() {
        return a*b;
    }
}
