package com.company.HomeWorks09;

import org.junit.Test;

import static org.junit.Assert.*;

public class FigureTest {

    @Test
    public void getPerimeter_Figure() {
        Figure figure = new Figure();
        assertEquals(0d,figure.getPerimeter(),1d/10000);
    }

    @Test
    public void getPerimeter_Ellipse() {
        Figure ellipse = new Ellipse(3,2);
        assertEquals(15.8796d,ellipse.getPerimeter(),1d/10000);
    }

    @Test
    public void getPerimeter_Circle() {
        Figure circle = new Circle(2);
        assertEquals(12.5663d,circle.getPerimeter(),1d/10000);
    }

    @Test
    public void getPerimeter_Rectangle() {
        Figure rectangle = new Rectangle(3,2);
        assertEquals(6d,rectangle.getPerimeter(),1d/10000);
    }

    @Test
    public void getPerimeter_Square() {
        Figure square = new Square(3);
        assertEquals(9d,square.getPerimeter(),1d/10000);
    }
}