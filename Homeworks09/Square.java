package com.company.HomeWorks09;

public class Square extends Rectangle{

    public Square(double a) {
        super(a,a);
    }

    @Override
    public double getPerimeter() {
        return Math.pow(getA(),2);
    }
}
