package com.company.HomeWorks20;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class AutoProcessor {
    private final String patchFile;

    public static void main(String[] args) {
        AutoProcessor autoProcessor = new AutoProcessor("Auto.txt");
        System.out.println(autoProcessor.getNumersWhereColorBlackAndMileageZero());
        System.out.println(autoProcessor.getUnicModelCounterWherePriceFrom700kTo800k());
        System.out.println(autoProcessor.getColorAutoWithMinimalPrice());
        System.out.println(autoProcessor.getAveragePriceOfCamry());
    }

    public AutoProcessor(String patchFile) {
        this.patchFile = patchFile;
    }

    public List<String> getNumersWhereColorBlackAndMileageZero() {
        return getStreamAuto()
                .filter(o -> o.color == Colors.BLACK && o.mileage == 0)
                .map(o -> o.numer)
                .collect(Collectors.toList());
    }

    public long getUnicModelCounterWherePriceFrom700kTo800k() {
        return getStreamAuto()
                .filter(o -> o.price >= 700000 && o.price <= 800000)
                .map(o -> o.model)
                .distinct()
                .count();
    }

    public String getColorAutoWithMinimalPrice() {
        return getStreamAuto()
                .min(Comparator.comparingInt(o -> o.price))
                .map(o -> o.color.toString())
                .orElse(null);
    }

    public double getAveragePriceOfCamry() {
        return getStreamAuto()
                .filter(o -> o.model.equals("Camry"))
                .collect(Collectors.averagingDouble(o -> o.price));
    }

    private Stream<Auto> getStreamAuto() {
        try {
            return Files.lines(Path.of(patchFile))
                    .map(o -> {
                        String[] parts = o.trim().split("\\|");
                        return new Auto(parts[0], parts[1], parts[2], Integer.parseInt(parts[3]), Integer.parseInt(parts[4]));
                    });
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public class Auto {
        private String numer, model;
        private Colors color;
        private int mileage, price;

        public Auto(String numer, String model, String color, int mileage, int price) {
            this.numer = numer;
            this.model = model;
            this.color = Colors.valueOf(color.toUpperCase(Locale.ROOT));
            this.mileage = mileage;
            this.price = price;
        }
    }

    private enum Colors {
        BLACK,
        RED,
        GREEN
    }
}
