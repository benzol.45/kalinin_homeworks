package com.company.HomeWorks20;

import org.junit.Test;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class AutoProcessorTest {

    @Test
    public void getNumersWhereColorBlackAndMileageZero() {
        List<String> expect = new ArrayList<>();
        String testFilePath;

        testFilePath = getTemplateFileWithData("");
        assertEquals(expect, new AutoProcessor(testFilePath).getNumersWhereColorBlackAndMileageZero());
        testFilePath = getTemplateFileWithData("o001aa111|Camry|Black|133|820000\no002aa111|Camry|Green|0|0\no003aa111|Corolla|Green|0|720000");
        assertEquals(expect, new AutoProcessor(testFilePath).getNumersWhereColorBlackAndMileageZero());

        testFilePath = getTemplateFileWithData("o001aa111|Camry|Black|0|820000\no002aa111|Camry|Green|0|0\no003aa111|Corolla|Green|0|720000");
        expect.clear();
        expect.add("o001aa111");
        assertEquals(expect, new AutoProcessor(testFilePath).getNumersWhereColorBlackAndMileageZero());

        testFilePath = getTemplateFileWithData("o001aa111|Camry|Black|0|820000\no002aa111|Camry|Green|0|0\no003aa111|Corolla|Black|0|720000");
        expect.clear();
        expect.add("o001aa111");
        expect.add("o003aa111");
        assertEquals(expect, new AutoProcessor(testFilePath).getNumersWhereColorBlackAndMileageZero());

        testFilePath = getTemplateFileWithData("o001aa111|Camry|Black|0|820000\no002aa111|Camry|Black|0|0\no003aa111|Corolla|Black|0|720000");
        expect.clear();
        expect.add("o001aa111");
        expect.add("o002aa111");
        expect.add("o003aa111");
        assertEquals(expect, new AutoProcessor(testFilePath).getNumersWhereColorBlackAndMileageZero());
    }

    @Test
    public void getUnicModelCounterWherePriceFrom700kTo800k() {
        String testFilePath;

        testFilePath = getTemplateFileWithData("");
        assertEquals(0, new AutoProcessor(testFilePath).getUnicModelCounterWherePriceFrom700kTo800k());

        testFilePath = getTemplateFileWithData("o001aa111|Camry|Black|133|820000\no002aa111|Camry|Green|0|0\no003aa111|Corolla|Green|0|920000");
        assertEquals(0, new AutoProcessor(testFilePath).getUnicModelCounterWherePriceFrom700kTo800k());

        testFilePath = getTemplateFileWithData("o001aa111|Camry|Black|133|820000\no002aa111|Camry|Green|0|0\no003aa111|Corolla|Green|0|720000");
        assertEquals(1, new AutoProcessor(testFilePath).getUnicModelCounterWherePriceFrom700kTo800k());

        testFilePath = getTemplateFileWithData("o001aa111|Camry|Black|133|820000\no002aa111|Camry|Green|0|799999\no003aa111|Corolla|Green|0|720000");
        assertEquals(2, new AutoProcessor(testFilePath).getUnicModelCounterWherePriceFrom700kTo800k());

        testFilePath = getTemplateFileWithData("o001aa111|Camry|Black|133|790000\no002aa111|Camry|Green|0|740000\no003aa111|Camry|Green|0|720000");
        assertEquals(1, new AutoProcessor(testFilePath).getUnicModelCounterWherePriceFrom700kTo800k());

        testFilePath = getTemplateFileWithData("o001aa111|Camry|Black|133|790000\no002aa111|Avensis|Green|0|740000\no003aa111|Corolla|Green|0|720000");
        assertEquals(3, new AutoProcessor(testFilePath).getUnicModelCounterWherePriceFrom700kTo800k());
    }

    @Test
    public void getColorAutoWithMinimalPrice() {
        String testFilePath;

        testFilePath = getTemplateFileWithData("");
        assertEquals(null, new AutoProcessor(testFilePath).getColorAutoWithMinimalPrice());

        testFilePath = getTemplateFileWithData("o001aa111|Camry|Black|133|0\no002aa111|Camry|Black|0|0\no003aa111|Corolla|Black|0|0");
        assertEquals("BLACK", new AutoProcessor(testFilePath).getColorAutoWithMinimalPrice());

        testFilePath = getTemplateFileWithData("o001aa111|Camry|Black|133|0\no002aa111|Camry|Green|0|0\no003aa111|Corolla|Green|0|0");
        assertEquals("BLACK", new AutoProcessor(testFilePath).getColorAutoWithMinimalPrice());

        testFilePath = getTemplateFileWithData("o001aa111|Camry|Black|133|3\no002aa111|Camry|Green|0|2\no003aa111|Corolla|Green|0|1");
        assertEquals("GREEN", new AutoProcessor(testFilePath).getColorAutoWithMinimalPrice());
    }

    @Test
    public void getAveragePriceOfCamry() {
        String testFilePath;

        testFilePath = getTemplateFileWithData("");
        assertEquals(0.0, new AutoProcessor(testFilePath).getAveragePriceOfCamry(), 0.0000000001);

        testFilePath = getTemplateFileWithData("o001aa111|Corolla|Black|133|0\no002aa111|Corolla|Black|0|0\no003aa111|Corolla|Black|0|0");
        assertEquals(0.0, new AutoProcessor(testFilePath).getAveragePriceOfCamry(), 0.0000000001);

        testFilePath = getTemplateFileWithData("o001aa111|Camry|Black|133|0\no002aa111|Corolla|Black|0|0\no003aa111|Corolla|Black|0|0");
        assertEquals(0.0, new AutoProcessor(testFilePath).getAveragePriceOfCamry(), 0.0000000001);

        testFilePath = getTemplateFileWithData("o001aa111|Camry|Black|133|0\no002aa111|Camry|Black|1|0\no003aa111|Camry|Black|3|0");
        assertEquals(0.0, new AutoProcessor(testFilePath).getAveragePriceOfCamry(), 0.0000000001);

        testFilePath = getTemplateFileWithData("o001aa111|Camry|Black|133|700000\no002aa111|Corolla|Black|1|0\no003aa111|Corolla|Black|3|0");
        assertEquals(700000.0, new AutoProcessor(testFilePath).getAveragePriceOfCamry(), 0.0000000001);

        testFilePath = getTemplateFileWithData("o001aa111|Camry|Black|133|700000\no002aa111|Camry|Black|1|0\no003aa111|Corolla|Black|3|0");
        assertEquals(350000.0, new AutoProcessor(testFilePath).getAveragePriceOfCamry(), 0.0000000001);

        testFilePath = getTemplateFileWithData("o001aa111|Camry|Black|133|700000\no002aa111|Camry|Black|1|800003\no003aa111|Camry|Black|3|900000");
        assertEquals(800001.0, new AutoProcessor(testFilePath).getAveragePriceOfCamry(), 0.0000000001);
    }

    private String getTemplateFileWithData(String data) {
        try {
            Path file = Files.createTempFile("TestFile", ".txt");
            Files.write(file, data.getBytes(StandardCharsets.UTF_8));
            return file.toFile().getAbsolutePath();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
