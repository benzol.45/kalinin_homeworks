package com.company;

/*На вход подается последовательность чисел, оканчивающихся на -1.

        Необходимо вывести число, которе присутствует в последовательности минимальное количество раз.

        Гарантируется:

        Все числа в диапазоне от -100 до 100.

        Числа встречаются не более 2 147 483 647-раз каждое.

        Сложность алгоритма - O(n)*/


import java.util.Arrays;
import java.util.stream.Collectors;

public class HomeWorks07 {

    public static byte getMinimalOccurrencesValue(byte[] array) {
        int endSequenceIndex=0;
        for (int i = 0; i < array.length; i++) {
            if (array[i]==-1) {
                endSequenceIndex=i;
                break;
            }
        }

        byte[] workArray = Arrays.copyOf(array,endSequenceIndex);
        if (workArray.length==0) {
            throw new IllegalArgumentException();
        }

          // НЕКОРРЕКТНО. Не укладывается в O(n) т.к. есть сортировка, сложность которой >O(n)
//        Arrays.sort(workArray);
//
//        byte minimalOccurrencesValue=101;
//        int minimalOccurrencesLength = workArray.length+1;
//        int currentOccurrencesLength = 1;
//        for (int i = 0; i < workArray.length; i++) {
//            if (i < workArray.length-1 && workArray[i]==workArray[i+1]) {
//                currentOccurrencesLength++;
//            } else {
//                if (currentOccurrencesLength<minimalOccurrencesLength) {
//                    minimalOccurrencesLength=currentOccurrencesLength;
//                    minimalOccurrencesValue = workArray[i];
//                }
//                currentOccurrencesLength=1;
//            }
//        }

        //return minimalOccurrencesValue;

        //КОРРЕКТНО. Укладывается в O(n) хотя и содежрит вложенный цикл. Количество проходов вложенного цикла не зависит от n.
        //Поэтому рост сложности пропорционален только n - увеличивается количество проходов только вложенного цикла.
        byte minimalOccurrencesValue=101;
        int minimalOccurrencesLength = workArray.length+1;

        int currentOccurrencesLength;
        for (byte i = -100; i <= 100 ; i++) {
            currentOccurrencesLength = 0;
            for (byte arrayValue: workArray) {
                if (arrayValue==i) {
                    currentOccurrencesLength++;
                }
            }
            if (currentOccurrencesLength<minimalOccurrencesLength) {
                minimalOccurrencesLength = currentOccurrencesLength;
                minimalOccurrencesValue = i;
            }
        }

        return minimalOccurrencesValue;
    }

    public static int getMinimalOccurrencesValueOverStream(int[] array) {
        if (Arrays.stream(array).filter(a->a==-1).findFirst().orElse(0)!=-1) {
            throw new IllegalArgumentException();
        }

        Integer result = Arrays.stream(array).boxed().takeWhile(a->a!=-1).collect(Collectors.toMap(a->a, a->1,(p, o)->p+o)).
                entrySet().stream().sorted((a1, a2)->(-1)*a1.getKey().compareTo(a2.getKey())).map(a->a.getKey()).findFirst().orElse(null);

        if (result!=null) {
            return (int)result;
        } else {
            throw new IllegalArgumentException();
        }
    }
}
