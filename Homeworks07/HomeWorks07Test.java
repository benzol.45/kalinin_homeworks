package com.company;

import org.junit.Test;

import static org.junit.Assert.*;

public class HomeWorks07Test {

    @Test (expected = IllegalArgumentException.class)
    public void getMinimalOccurrencesValue() {
        HomeWorks07.getMinimalOccurrencesValue(new byte[]{1,1,1,1});
        HomeWorks07.getMinimalOccurrencesValue(new byte[]{-1});
        assertEquals(1,HomeWorks07.getMinimalOccurrencesValue(new byte[]{1,1,1,-1,2,2}));
        assertEquals(7,HomeWorks07.getMinimalOccurrencesValue(new byte[]{1,1,1,2,2,2,3,3,3,4,4,4,5,5,5,5,6,6,6,7,7,-1}));
        assertEquals(6,HomeWorks07.getMinimalOccurrencesValue(new byte[]{1,1,1,2,2,2,3,3,3,4,4,4,5,5,5,5,6,6,7,7,7,-1}));
        assertEquals(1,HomeWorks07.getMinimalOccurrencesValue(new byte[]{1,1,1,-1}));
        assertEquals(6,HomeWorks07.getMinimalOccurrencesValue(new byte[]{1,6,1,2,3,1,2,3,-1,1}));

    }

    @Test (expected = IllegalArgumentException.class)
    public void getMinimalOccurrencesValueOverStream() {
        HomeWorks07.getMinimalOccurrencesValueOverStream(new int[]{1,1,1,1});
        HomeWorks07.getMinimalOccurrencesValueOverStream(new int[]{-1});
        assertEquals(1,HomeWorks07.getMinimalOccurrencesValueOverStream(new int[]{1,1,1,-1,2,2}));
        assertEquals(7,HomeWorks07.getMinimalOccurrencesValueOverStream(new int[]{1,1,1,2,2,2,3,3,3,4,4,4,5,5,5,5,6,6,6,7,7,-1}));
        assertEquals(6,HomeWorks07.getMinimalOccurrencesValueOverStream(new int[]{1,1,1,2,2,2,3,3,3,4,4,4,5,5,5,5,6,6,7,7,7,-1}));
        assertEquals(1,HomeWorks07.getMinimalOccurrencesValueOverStream(new int[]{1,1,1,-1}));
        assertEquals(6,HomeWorks07.getMinimalOccurrencesValueOverStream(new int[]{1,6,1,2,3,1,2,3,-1,1}));
    }
}