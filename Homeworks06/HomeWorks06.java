package com.company;

/*Реализовать функцию, принимающую на вход массив и целое число. Данная функция должна вернуть индекс этого числа в массиве. Если число в массиве отсутствует - вернуть -1.

 Реализовать процедуру, которая переместит все значимые элементы влево, заполнив нулевые, например:
        было:
        34, 0, 0, 14, 15, 0, 18, 0, 0, 1, 20
        стало
        34, 14, 15, 18, 1, 20, 0, 0, 0, 0*/

import java.util.Arrays;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class HomeWorks06 {
    public static void main(String[] args) {
    }

    public static int getValueIndexInArray(int value, int[] array) {
        return IntStream.range(0,array.length).boxed().collect(Collectors.toMap(i->i,i->array[i])).
                entrySet().stream().filter(a->a.getValue()==value).map(a->a.getKey()).findFirst().orElse(-1);
    }

    public static void moveNonNullValueInArray(int[] array) {
        final int arrayLength = array.length;
        int[] newArray = Arrays.stream(Arrays.stream(array).filter(a -> a != 0).boxed().toArray(a -> new Integer[arrayLength])).mapToInt(a->(a==null)?0:a).toArray();
        System.arraycopy(newArray,0,array,0,arrayLength);
    }
}
