package com.company;

import org.junit.Test;

import java.util.Arrays;

import static org.junit.Assert.*;

public class HomeWorks06Test {

    @Test
    public void getValueIndexInArray() {
        int[] testArray = {1,2,3,3,4,5,6,4,7,8,9,10};
        int[] emptyArray = {};
        assertEquals(-1, HomeWorks06.getValueIndexInArray(1, emptyArray));
        assertEquals(-1, HomeWorks06.getValueIndexInArray(0, testArray));
        assertEquals(1, HomeWorks06.getValueIndexInArray(2, testArray));
        assertEquals(2, HomeWorks06.getValueIndexInArray(3, testArray));
        assertEquals(4, HomeWorks06.getValueIndexInArray(4, testArray));
    }

    @Test
    public void moveNonNullValueInArray() {
        int[] emtyArray = {};
        HomeWorks06.moveNonNullValueInArray(emtyArray);
        assertTrue(Arrays.equals(emtyArray,emtyArray));

        int[] oneZeroArray = {0};
        HomeWorks06.moveNonNullValueInArray(oneZeroArray);
        assertTrue(Arrays.equals(oneZeroArray,oneZeroArray));

        int[] onlyZeroArray = {0,0,0,0};
        HomeWorks06.moveNonNullValueInArray(onlyZeroArray);
        assertTrue(Arrays.equals(onlyZeroArray,onlyZeroArray));

        int[] oneNumerArray = {0,0,0,0,9};
        HomeWorks06.moveNonNullValueInArray(oneNumerArray);
        assertTrue(Arrays.equals(new int[]{9,0,0,0,0},oneNumerArray));

        int[] testArray = {0,1,2,0,3,4,0,0};
        HomeWorks06.moveNonNullValueInArray(testArray);
        assertTrue(Arrays.equals(new int[]{1,2,3,4,0,0,0,0},testArray));

        testArray = new int[]{34,0,0,14,15,0,18,0,0,1,20};
        HomeWorks06.moveNonNullValueInArray(testArray);
        assertTrue(Arrays.equals(new int[]{34,14,15,18,1,20,0,0,0,0,0},testArray));
    }
}