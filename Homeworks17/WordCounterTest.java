package com.company.HomeWorks17;

import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.*;

public class WordCounterTest {

    @Test
    public void countWord() {

        Map<String, Integer> expectedMap = new HashMap<>();
        assertEquals(expectedMap, WordCounter.countWord(""));
        assertEquals(expectedMap, WordCounter.countWord(" "));
        assertEquals(expectedMap, WordCounter.countWord("Мама"));
        assertEquals(expectedMap, WordCounter.countWord(" Мама"));
        assertEquals(expectedMap, WordCounter.countWord("Мама "));
        assertEquals(expectedMap, WordCounter.countWord("Мама     "));

        expectedMap.put("Мама", 1);
        assertEquals(expectedMap, WordCounter.countWord(" Мама "));
        assertEquals(expectedMap, WordCounter.countWord(" Мама       "));

        expectedMap.clear();
        expectedMap.put("мыла", 1);
        assertEquals(expectedMap, WordCounter.countWord("Мама мыла раму"));
        assertEquals(expectedMap, WordCounter.countWord("Мама мыла        раму"));

        expectedMap.clear();
        expectedMap.put("Мама", 1);
        expectedMap.put("мыла", 1);
        assertEquals(expectedMap, WordCounter.countWord(" Мама мыла раму"));
        assertEquals(expectedMap, WordCounter.countWord(" Мама мыла        раму"));

        expectedMap.clear();
        expectedMap.put("мыла", 1);
        expectedMap.put("раму", 1);
        assertEquals(expectedMap, WordCounter.countWord("Мама мыла раму "));
        assertEquals(expectedMap, WordCounter.countWord("Мама мыла    раму "));

        expectedMap.clear();
        expectedMap.put("Мама", 1);
        expectedMap.put("мыла", 1);
        expectedMap.put("раму", 1);
        assertEquals(expectedMap, WordCounter.countWord(" Мама мыла раму "));

        expectedMap.clear();
        expectedMap.put("Мама", 1);
        expectedMap.put("мыла", 1);
        expectedMap.put("Раму", 1);
        expectedMap.put("и", 1);
        expectedMap.put("раму", 1);
        assertEquals(expectedMap, WordCounter.countWord(" Мама мыла Раму и раму "));

        expectedMap.clear();
        expectedMap.put("Мама", 1);
        expectedMap.put("мыла", 1);
        expectedMap.put("раму,", 1);
        expectedMap.put("раму", 1);
        assertEquals(expectedMap, WordCounter.countWord(" Мама мыла раму, раму "));
    }

    @Test
    public void countWordOverMerge() {

        Map<String, Integer> expectedMap = new HashMap<>();
        assertEquals(expectedMap, WordCounter.countWordOverMerge(""));
        assertEquals(expectedMap, WordCounter.countWordOverMerge(" "));
        assertEquals(expectedMap, WordCounter.countWordOverMerge("Мама"));
        assertEquals(expectedMap, WordCounter.countWordOverMerge(" Мама"));
        assertEquals(expectedMap, WordCounter.countWordOverMerge("Мама "));
        assertEquals(expectedMap, WordCounter.countWordOverMerge("Мама      "));

        expectedMap.put("Мама", 1);
        assertEquals(expectedMap, WordCounter.countWordOverMerge(" Мама "));
        assertEquals(expectedMap, WordCounter.countWordOverMerge(" Мама      "));

        expectedMap.clear();
        expectedMap.put("мыла", 1);
        assertEquals(expectedMap, WordCounter.countWordOverMerge("Мама мыла раму"));

        expectedMap.clear();
        expectedMap.put("Мама", 1);
        expectedMap.put("мыла", 1);
        assertEquals(expectedMap, WordCounter.countWordOverMerge(" Мама мыла раму"));
        assertEquals(expectedMap, WordCounter.countWordOverMerge(" Мама мыла     раму"));

        expectedMap.clear();
        expectedMap.put("мыла", 1);
        expectedMap.put("раму", 1);
        assertEquals(expectedMap, WordCounter.countWordOverMerge("Мама мыла раму "));
        assertEquals(expectedMap, WordCounter.countWordOverMerge("Мама мыла    раму "));

        expectedMap.clear();
        expectedMap.put("Мама", 1);
        expectedMap.put("мыла", 1);
        expectedMap.put("раму", 1);
        assertEquals(expectedMap, WordCounter.countWordOverMerge(" Мама мыла раму "));

        expectedMap.clear();
        expectedMap.put("Мама", 1);
        expectedMap.put("мыла", 1);
        expectedMap.put("Раму", 1);
        expectedMap.put("и", 1);
        expectedMap.put("раму", 1);
        assertEquals(expectedMap, WordCounter.countWordOverMerge(" Мама мыла Раму и раму "));

        expectedMap.clear();
        expectedMap.put("Мама", 1);
        expectedMap.put("мыла", 1);
        expectedMap.put("раму,", 1);
        expectedMap.put("раму", 1);
        assertEquals(expectedMap, WordCounter.countWordOverMerge(" Мама мыла раму, раму "));
    }
}
