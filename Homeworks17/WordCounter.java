package com.company.HomeWorks17;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

public class WordCounter {

    public static void main(String[] args) {
        System.out.println(countWord("So Long, and Thanks for all the Fish. So Long, and Thanks for all the Fish. So Long, and Thanks for all the Fish").toString());
    }

    public static Map<String, Integer> countWord(String inputString) {
        boolean firstIsWord = inputString.startsWith(" ");
        boolean lastIsWord = inputString.endsWith(" ");

        String[] strings = Arrays.stream(inputString.trim().split(" "))
            .filter(o->!o.equals(""))
            .toArray(String[]::new);
        int counterFrom = firstIsWord ? 0 : 1;
        int counterTo = lastIsWord ? strings.length : strings.length - 1;

        return Arrays.stream(strings)
            .filter(o->!o.equals(""))
            .skip(counterFrom)
            .limit((counterTo < counterFrom) ? 0 : counterTo - counterFrom)
            .collect(Collectors.toMap(o -> o, o -> 1, (o, p) -> o + p));
    }

    public static Map<String, Integer> countWordOverMerge(String inputString) {
        Map<String, Integer> counterMap = new HashMap<>();

        boolean firstIsWord = inputString.startsWith(" ");
        boolean lastIsWord = inputString.endsWith(" ");

        String[] strings = inputString.trim().split(" ");
        int counterFrom = firstIsWord ? 0 : 1;
        int counterTo = lastIsWord ? strings.length : strings.length - 1;
        if (counterTo < counterFrom) {
            return counterMap;
        }

        for (int i = counterFrom; i < counterTo; i++) {
            if (!strings[i].equals("")) {
                 counterMap.merge(strings[i], 1, (o, one) -> o + one);
            }
        }

        return counterMap;
    }
}
