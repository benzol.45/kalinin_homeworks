package com.company.Attestation01;

import org.junit.Test;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.stream.Collectors;

import static org.junit.Assert.*;

public class UsersRepositoryFileImplTest {

    @Test
    public void findById() {
        String testedFile;
        User expectedUser;

        testedFile = getTestFilePath("");
        assertEquals(null, new UsersRepositoryFileImpl(testedFile).findById(1));

        testedFile = getTestFilePath("1|Игорь|33|true");
        assertEquals(null, new UsersRepositoryFileImpl(testedFile).findById(10));
        expectedUser = new User(1, "Игорь", 33, true);
        assertEquals(expectedUser, new UsersRepositoryFileImpl(testedFile).findById(1));

        testedFile = getTestFilePath("1|Игорь|33|true\n5|Петр|29|true\n4|Андрей|29|true");
        assertEquals(null, new UsersRepositoryFileImpl(testedFile).findById(2));
        expectedUser = new User(5, "Петр", 29, true);
        assertEquals(expectedUser, new UsersRepositoryFileImpl(testedFile).findById(5));

        testedFile = getTestFilePath("1|Игорь|33|true\n1|Петр|29|true\n1|Андрей|29|true");
        expectedUser = new User(1, "Игорь", 33, true);
        assertEquals(expectedUser, new UsersRepositoryFileImpl(testedFile).findById(1));
    }

    @Test
    public void update() {
        String testedString = "1|Игорь|33|true\n5|Петр|29|true\n4|Андрей|29|true";
        String testedFile = getTestFilePath(testedString);
        UsersRepositoryFileImpl usersRepositoryFile = new UsersRepositoryFileImpl(testedFile);
        User user = new User(2, "Иван", 37, false);
        usersRepositoryFile.update(user);
        testedString = "";
        try {
            testedString = Files.lines(Path.of(testedFile)).collect(Collectors.joining("\n"));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        String expectedString = "1|Игорь|33|true\n5|Петр|29|true\n4|Андрей|29|true";
        assertEquals(expectedString, testedString);


        testedString = "1|Игорь|33|true\n1|Петр|29|true\n1|Андрей|29|true";
        testedFile = getTestFilePath(testedString);
        usersRepositoryFile = new UsersRepositoryFileImpl(testedFile);
        user = usersRepositoryFile.findById(1);
        user.setName("Иван");
        user.setAge(37);
        user.setWorker(false);
        usersRepositoryFile.update(user);
        testedString = "";
        try {
            testedString = Files.lines(Path.of(testedFile)).collect(Collectors.joining("\n"));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        expectedString = "1|Иван|37|false\n1|Иван|37|false\n1|Иван|37|false";
        assertEquals(expectedString, testedString);


        testedString = "1|Игорь|33|true\n5|Петр|29|true\n4|Андрей|29|true";
        testedFile = getTestFilePath(testedString);
        usersRepositoryFile = new UsersRepositoryFileImpl(testedFile);
        user = usersRepositoryFile.findById(5);
        user.setName("Иван");
        user.setAge(37);
        user.setWorker(false);
        usersRepositoryFile.update(user);
        testedString = "";
        try {
            testedString = Files.lines(Path.of(testedFile)).collect(Collectors.joining("\n"));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        expectedString = "1|Игорь|33|true\n5|Иван|37|false\n4|Андрей|29|true";
        assertEquals(expectedString, testedString);


        testedString = "1|Игорь|33|true\n5|Петр|29|true\n4|Андрей|29|true";
        testedFile = getTestFilePath(testedString);
        usersRepositoryFile = new UsersRepositoryFileImpl(testedFile);
        user = usersRepositoryFile.findById(5);
        user = new User(5, "Иван", 37, false);
        usersRepositoryFile.update(user);
        testedString = "";
        try {
            testedString = Files.lines(Path.of(testedFile)).collect(Collectors.joining("\n"));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        expectedString = "1|Игорь|33|true\n5|Иван|37|false\n4|Андрей|29|true";
        assertEquals(expectedString, testedString);
    }

    private String getTestFilePath(String testData) {
        try {
            Path file = Files.createTempFile("testFile", ".txt");
            Files.write(file, testData.getBytes(StandardCharsets.UTF_8));
            return file.toFile().getAbsolutePath();
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }
}