package com.company.Attestation01;


import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.stream.Collectors;
import java.util.stream.Stream;


public class UsersRepositoryFileImpl implements UsersRepository {

    private final String fileName;

    public UsersRepositoryFileImpl(String fileName) {
        this.fileName = fileName;
    }

    @Override
    public User findById(int id) {
        return getUserStreamFromfile()
                .filter(o -> o.getId() == id)
                .findFirst()
                .orElse(null);
    }

    @Override
    public void update(User user) {
        String newFileData = getUserStreamFromfile()
                .map(o -> {
                    if (o.getId() == user.getId() && !o.equals(user)) {
                        return user;
                    } else {
                        return o;
                    }
                })
                .map(o -> getStringOfUser(o))
                .collect(Collectors.joining("\n"));
        try {
            Files.write(Path.of(fileName), newFileData.getBytes(StandardCharsets.UTF_8));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private Stream<User> getUserStreamFromfile() {
        try {
            return Files.lines(Path.of(this.fileName))
                    .map(o -> getUserFromString(o));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private User getUserFromString(String string) {
        String[] parts = string.trim().split("\\|");
        return new User(Integer.parseInt(parts[0]), parts[1], Integer.parseInt(parts[2]), Boolean.parseBoolean(parts[3]));
    }

    private String getStringOfUser(User user) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(user.getId()).append("|").append(user.getName()).append("|").append(user.getAge()).append("|").append(user.isWorker());
        return stringBuilder.toString();
    }
}
