package com.company.Attestation01;

public interface UsersRepository {
    User findById(int id);

    void update(User user);

}
