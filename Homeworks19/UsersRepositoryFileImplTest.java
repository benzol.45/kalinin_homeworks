package com.company.HomeWorks19;

import org.junit.Test;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import static org.junit.Assert.*;

public class UsersRepositoryFileImplTest {

    @Test
    public void findByAge() {
        String path = getTestFilePath("");
        List<User> expectedList = new ArrayList<>();
        assertEquals(convertToUsersequaliterList(expectedList), convertToUsersequaliterList(new UsersRepositoryFileImpl(path).findByAge(0)));
        assertEquals(convertToUsersequaliterList(expectedList), convertToUsersequaliterList(new UsersRepositoryFileImpl(path).findByAge(10)));
        assertEquals(convertToUsersequaliterList(expectedList), convertToUsersequaliterList(new UsersRepositoryFileImpl(path).findByAge(-10)));

        path = getTestFilePath("adam|50|false\neva|49|false\nkain|20|true\navel|15|true");
        assertEquals(convertToUsersequaliterList(expectedList), convertToUsersequaliterList(new UsersRepositoryFileImpl(path).findByAge(0)));
        assertEquals(convertToUsersequaliterList(expectedList), convertToUsersequaliterList(new UsersRepositoryFileImpl(path).findByAge(10)));
        assertEquals(convertToUsersequaliterList(expectedList), convertToUsersequaliterList(new UsersRepositoryFileImpl(path).findByAge(-10)));

        expectedList.add(new User("eva", 49, false));
        assertEquals(convertToUsersequaliterList(expectedList), convertToUsersequaliterList(new UsersRepositoryFileImpl(path).findByAge(49)));
        expectedList.clear();
        expectedList.add(new User("adam", 50, false));
        assertEquals(convertToUsersequaliterList(expectedList), convertToUsersequaliterList(new UsersRepositoryFileImpl(path).findByAge(50)));
        expectedList.clear();
        expectedList.add(new User("avel", 15, true));
        assertEquals(convertToUsersequaliterList(expectedList), convertToUsersequaliterList(new UsersRepositoryFileImpl(path).findByAge(15)));

        path = getTestFilePath("adam|50|false\neva|50|false\nkain|20|true\navel|15|true");
        expectedList.clear();
        expectedList.add(new User("adam", 50, false));
        expectedList.add(new User("eva", 50, false));
        assertEquals(convertToUsersequaliterList(expectedList), convertToUsersequaliterList(new UsersRepositoryFileImpl(path).findByAge(50)));

        path = getTestFilePath("adam|50|false\neva|50|false\nkain|50|true\navel|50|true");
        expectedList.clear();
        expectedList.add(new User("adam", 50, false));
        expectedList.add(new User("eva", 50, false));
        expectedList.add(new User("kain", 50, true));
        expectedList.add(new User("avel", 50, true));
        assertEquals(convertToUsersequaliterList(expectedList), convertToUsersequaliterList(new UsersRepositoryFileImpl(path).findByAge(50)));
    }

    @Test
    public void findByIsWorkerIsTrue() {
        String path = getTestFilePath("");
        List<User> expectedList = new ArrayList<>();
        assertEquals(convertToUsersequaliterList(expectedList), convertToUsersequaliterList(new UsersRepositoryFileImpl(path).findByIsWorkerIsTrue()));

        path = getTestFilePath("adam|50|false\neva|49|false\nkain|20|false\navel|15|false");
        assertEquals(convertToUsersequaliterList(expectedList), convertToUsersequaliterList(new UsersRepositoryFileImpl(path).findByIsWorkerIsTrue()));

        path = getTestFilePath("adam|50|true\neva|49|false\nkain|20|false\navel|15|false");
        expectedList.clear();
        expectedList.add(new User("adam", 50, true));
        assertEquals(convertToUsersequaliterList(expectedList), convertToUsersequaliterList(new UsersRepositoryFileImpl(path).findByIsWorkerIsTrue()));

        path = getTestFilePath("adam|50|false\neva|49|false\nkain|20|false\navel|15|true");
        expectedList.clear();
        expectedList.add(new User("avel", 15, true));
        assertEquals(convertToUsersequaliterList(expectedList), convertToUsersequaliterList(new UsersRepositoryFileImpl(path).findByIsWorkerIsTrue()));

        path = getTestFilePath("adam|50|false\neva|49|true\nkain|20|false\navel|15|false");
        expectedList.clear();
        expectedList.add(new User("eva", 49, true));
        assertEquals(convertToUsersequaliterList(expectedList), convertToUsersequaliterList(new UsersRepositoryFileImpl(path).findByIsWorkerIsTrue()));

        path = getTestFilePath("adam|50|true\neva|49|true\nkain|20|false\navel|15|false");
        expectedList.clear();
        expectedList.add(new User("adam", 50, true));
        expectedList.add(new User("eva", 49, true));
        assertEquals(convertToUsersequaliterList(expectedList), convertToUsersequaliterList(new UsersRepositoryFileImpl(path).findByIsWorkerIsTrue()));

        path = getTestFilePath("adam|50|true\neva|49|true\nkain|20|true\navel|15|true");
        expectedList.clear();
        expectedList.add(new User("adam", 50, true));
        expectedList.add(new User("eva", 49, true));
        expectedList.add(new User("kain", 20, true));
        expectedList.add(new User("avel", 15, true));
        assertEquals(convertToUsersequaliterList(expectedList), convertToUsersequaliterList(new UsersRepositoryFileImpl(path).findByIsWorkerIsTrue()));
    }

    private String getTestFilePath(String testData) {
        try {
            Path file = Files.createTempFile("testFile", ".txt");
            Files.write(file, testData.getBytes(StandardCharsets.UTF_8));
            return file.toFile().getAbsolutePath();
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    private List<Usersequaliter> convertToUsersequaliterList(List<User> userList) {
        return userList.stream().map(o -> new Usersequaliter(o)).collect(Collectors.toList());
    }

    private class Usersequaliter {
        final private User user;

        public Usersequaliter(User user) {
            this.user = user;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Usersequaliter that = (Usersequaliter) o;
            if (that.user == null || user.getClass() != ((Usersequaliter) o).user.getClass()) return false;

            return user.getAge() == that.user.getAge() && user.isWorker() == that.user.isWorker() && Objects.equals(user.getName(), that.user.getName());
        }

        @Override
        public int hashCode() {
            return Objects.hash(user);
        }
    }
}
