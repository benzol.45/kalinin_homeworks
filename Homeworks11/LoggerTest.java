package com.company.HomeWorks11;

import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.junit.Assert.*;

public class LoggerTest {

    @Test
    public void getInstance() {
        Logger logger1=Logger.getInstance();
        Logger logger2=Logger.getInstance();
        assertTrue(logger1==logger2);
    }

    @Test
    public void log() {
        String expectedMessage = "Test\nTest 01";

        PrintStream standartSystemOut = System.out;
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        PrintStream toByteArray = new PrintStream(byteArrayOutputStream);
        System.setOut(toByteArray);

        Logger.getInstance().log(expectedMessage);
        System.out.flush();
        String consoleMessage = byteArrayOutputStream.toString();
        System.setOut(standartSystemOut);

        consoleMessage = consoleMessage.trim(); //Because in Logger use println()

        assertEquals(expectedMessage,consoleMessage);
    }
}