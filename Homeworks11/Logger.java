package com.company.HomeWorks11;

public class Logger {
    private static final Logger instance;

    private Logger(){}

    static {
        instance=new Logger();
    }

    public static Logger getInstance() {
        return instance;
    }

    public void log(String message) {
        System.out.println(message);
    }

}
