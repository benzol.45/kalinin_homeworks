package com.company.HomeWorks08;

/*На вход подается информация о людях в количестве 10 человек (имя - строка, вес - вещественное число).

        Считать эти данные в массив объектов.

        Вывести в отсортированном по возрастанию веса порядке.*/


import java.util.Arrays;

public class HomeWorks08 {

    public static void sortHumensArrayOverComparator(Humen[] humens) {
        Arrays.sort(humens);
    }

    public static void sortHumensArrayOverSelection(Humen[] humens) {
        for (int i = 0; i < humens.length; i++) {
            Humen minimalHumen=humens[i];
            for (int j = i+1; j < humens.length; j++) {
                if (humens[j].getWeight()<minimalHumen.getWeight()) {
                    Humen temp = minimalHumen;
                    minimalHumen = humens[j];
                    humens[j] = temp;
                }
            }
            humens[i]=minimalHumen;
        }
    }

}
