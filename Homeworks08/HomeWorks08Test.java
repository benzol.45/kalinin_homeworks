package com.company.HomeWorks08;

import org.junit.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.*;

public class HomeWorks08Test {

    @Test
    public void sortHumensArrayOverComparator() {
        Humen[] expectedHumens = getHumensArray();
        Humen[] suffffledHumens = getRandomizeHumensArray(expectedHumens);
        Humen[] actualHumens = Arrays.copyOf(suffffledHumens,suffffledHumens.length);
        HomeWorks08.sortHumensArrayOverComparator(actualHumens);
        assertArrayEquals(expectedHumens,actualHumens);

        Humen[] emptyHumens = new Humen[0];
        Humen[] actualEmptyHumens = Arrays.copyOf(emptyHumens,emptyHumens.length);
        HomeWorks08.sortHumensArrayOverComparator(actualEmptyHumens);
        assertArrayEquals(emptyHumens,actualEmptyHumens);

        Humen[] oneHumen = new Humen[1];
        oneHumen[0] = new Humen("humen",1f);
        Humen[] actualOneHumens = Arrays.copyOf(oneHumen,oneHumen.length);
        HomeWorks08.sortHumensArrayOverComparator(actualOneHumens);
        assertArrayEquals(oneHumen,actualOneHumens);

    }

    @Test
    public void sortHumensArrayOverSelection() {
        Humen[] expectedHumens = getHumensArray();
        Humen[] suffffledHumens = getRandomizeHumensArray(expectedHumens);
        Humen[] actualHumens = Arrays.copyOf(suffffledHumens,suffffledHumens.length);
        HomeWorks08.sortHumensArrayOverSelection(actualHumens);
        assertArrayEquals(expectedHumens,actualHumens);

        Humen[] emptyHumens = new Humen[0];
        Humen[] actualEmptyHumens = Arrays.copyOf(emptyHumens,emptyHumens.length);
        HomeWorks08.sortHumensArrayOverSelection(actualEmptyHumens);
        assertArrayEquals(emptyHumens,actualEmptyHumens);

        Humen[] oneHumen = new Humen[1];
        oneHumen[0] = new Humen("humen",1f);
        Humen[] actualOneHumens = Arrays.copyOf(oneHumen,oneHumen.length);
        HomeWorks08.sortHumensArrayOverSelection(actualOneHumens);
        assertArrayEquals(oneHumen,actualOneHumens);
    }

    private Humen[] getHumensArray() {
        Humen[] humens = new Humen[10];
        for (int i = 0; i < 10; i++) {
            humens[i] = new Humen("Humen_"+i,1.1f*i);
        }
        return humens;
    }

    private Humen[] getRandomizeHumensArray(Humen[] sortedHumens) {
        Humen[] suffffledHumens = Arrays.copyOf(sortedHumens,sortedHumens.length);
        List<Humen> humenList = Arrays.asList(suffffledHumens);
        Collections.shuffle(humenList);
        return (humenList.toArray(suffffledHumens));
    }
}