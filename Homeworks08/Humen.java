package com.company.HomeWorks08;

import java.util.Objects;

public class Humen implements Comparable{
    private String name;
    private float weight;

    public Humen(String name, float weight) {
        this.name = name;
        this.weight = weight;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float getWeight() {
        return weight;
    }

    public void setWeight(float weight) {
        this.weight = weight;
    }

    @Override
    public int compareTo(Object o) {
        if (getClass()!=o.getClass()) {
            throw new IllegalArgumentException();
        }

        Humen anotherHumen = (Humen) o;
        return ((Float)weight).compareTo(anotherHumen.weight);
    }

    @Override
    public String toString() {
        return "Humen{" +
                "name='" + name + '\'' +
                ", weight=" + weight +
                '}';
    }
}
